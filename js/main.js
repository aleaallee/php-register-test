$(document).on("submit", "form.js-register", function(event){
    event.preventDefault();
    
    var _form = $(this);
    var _error = $(".js-error", _form);
    var data = {
        email : $("input[type='email']", _form).val(),
        password : $("input[type='password']", _form).val()
    };
    
    if(data.email.length < 6){
        _error.text("Please, enter a valid email adress").show();
        return false;
    }else if(data.password.length < 6){
        _error.text("Please, enter a passphrase that is at least 6 characters long ").show();
        return false;
    }
    
    _error.hide();
    
    $.ajax({
        type: 'POST',
        url: '/ajax/register.php',
        data: data,
        dataType: 'json'
    })
    .done(function ajaxDone(data){
        console.log(data);
        if(data.redirect !== undefined){
            window.location = data.redirect;
        }else if(data.error !== undefined){
            _error.text(data.error).show();
        }
    })
    .fail(function ajaxFailed(e){
        
    })
    .always(function ajaxAlwaysDoThis(e){
        console.log('Always: ');
    });

    return false;
});
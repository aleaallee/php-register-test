<?php
if(!defined('__CONFIG__')){
    exit('You do not have config file.');
}
class DB{
    protected static $con;
    private function __construct(){
        try{
            self::$con = new PDO('mysql:charset=utf8;host=192.168.0.158;port=3306;dbname=login', 'remote', 'remote');
            self::$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$con->setAttribute(PDO::ATTR_PERSISTENT, false);
        }catch(PDOException $e){
            echo "Could not connect to database"; exit;
        }
    }
    public static function getConnection(){
        //If this instance has not been started, start it
        if(!self::$con){
            new DB();
        }
        // Return the writeable DB Connection
        return self::$con;
    }
}
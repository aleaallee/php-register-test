<?php
    //if there is config constant defined then don't load this file.
    if(!defined('__CONFIG__')){
        exit('You do not have config file.');
    }


    //Allow errors
    error_reporting(-1);
    ini_set('display_errors', 'On');

    define('ALLOW_FOOTER', true);
    //include db fle
    include_once "classes/db.php";
    include_once "classes/filter.php";

    $con = db::getConnection();
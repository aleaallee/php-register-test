<?php
    //Allow the config 
    define('__CONFIG__', true);
    //Require the config
    require_once "inc/config.php";
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Php Ajax Register login test</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/uikit.min.css">
</head>
<body>
    <div class="uk-selection uk-container">
        <?php 
            echo "<p>Hello World, today is: ".date('d-m-Y')."</p>";
        ?>
        <p>
            <a href="login.php">Login</a>
            <a href="register.php">Register</a>
        </p>
    </div>
    
    <?php require_once "inc/footer.php";?>
</body>

</html>
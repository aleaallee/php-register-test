<?php
    define("__CONFIG__", true);
    require_once("../inc/config.php");
    require_once("../inc/classes/filter.php");
    
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        header("Content-Type: application/json");
        $return = [];
        
        $email = Filter::String($_POST['email']);

        //make sure the user doesn't exists
        $finduser = $con->prepare("SELECT user_id FROM users WHERE email = LOWER(:email) LIMIT 1");
        $finduser->bindParam(':email', $email, PDO::PARAM_STR);
        $finduser->execute();
        if($finduser->rowCount() == 1){
            //User exists
            //We can also check to see if they are able to log in.
            $return['error'] = 'You already have an account';
            $return['is_logged_in'] = false;
        }else{
            //User doesn't exists, add them now

            $password = password_hash($_POST['password'], PASSWORD_DEFAULT);

            $addUser = $con->prepare("INSERT INTO users(email, password) VALUES(LOWER(:email), :password);");
            $addUser->bindParam(':email', $email, PDO::PARAM_STR);
            $addUser->bindParam(':password', $password, PDO::PARAM_STR);
            $addUser->execute();

            $user_id = $con->lastInsertID();

            $_SESSION['user_id'] == (int) $user_id;

            $return['redirect'] ='/dashboard.php?message=Welcome';
            $return['is_logged_in'] = false;
        }
        $return["name"] = "Alejandro Esquivel";

        echo json_encode($return, JSON_PRETTY_PRINT);exit;
    }else{
        exit("Invalid URL");
    }
?>
<?php
    //Allow the config 
    define('__CONFIG__', true);
    //Require the config
    require_once "inc/config.php";
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Php Ajax Register login test</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/uikit.min.css">
</head>
<body>
    <div class="uk-selection uk-container">
        <div class="uk-grid uk-child-width-1-3@s uk-child-width-1-1" uk-grid>
            <form class="uk-form-stacked js-login">
                <h2>Login</h2>
                <div class="uk-margin">
                    <label class="uk-form-label" for="form-stacked-text">Email</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" id="form-stacked-text" required="required" type="email" autocomplete="Email" placeholder="Email">
                    </div>
                </div>
                <div class="uk-margin">
                    <label class="uk-form-label" for="form-stacked-text">Password</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" id="form-stacked-text" required="required" type="password" autocomplete="current-password" placeholder="Password">
                    </div>
                </div>
                <div class="uk-margin">
                    <button type="submit" class="uk-button uk-button-default">Login</button>
                </div>
            </form>
        </div>
    </div>
    <?php require_once "inc/footer.php";?>
</body>

</html>